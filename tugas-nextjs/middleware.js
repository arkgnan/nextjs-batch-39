import { NextRequest, NextResponse } from "next/server";

export function middleware(req, res) {
  if (req.nextUrl.pathname.startsWith("/server-side")) {
    if (req.cookies.get("token") === undefined) {
      return NextResponse.redirect(new URL("/login", req.url));
    }
  }
}
