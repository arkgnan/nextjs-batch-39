import React from "react";
import { useForm } from "react-hook-form";
import Cookies from "js-cookie";
import axios from "axios";
import { useRouter } from "next/router";
import { ButtonComponent } from "../../components/ButtonComponent";
import { InputComponent } from "../../components/InputComponent";
import Layout from "../../widget/Layout";

function login(props) {
  const router = useRouter();
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
    clearErrors,
  } = useForm();
  const email = register("email", { required: "Email is required" });
  const password = register("password", { required: "Password is required" });
  const onSubmit = async (form) => {
    const { data } = await axios
      .post(`https://backendexample.sanbersy.com/api/user-login`, form)
      .catch(function (error) {
        if (error.response) {
          throw new Error(error.response.data);
        }
      });
    Cookies.set("token", data.token, { expires: 1 });
    router.push("/server-side");
    return data;
  };
  return (
    <Layout>
      <form className="grid w-full h-screen place-items-center">
        <div className="w-3/12">
          <InputComponent
            placeholder="Your email"
            name={email.name}
            onChange={email.onChange}
            onBlur={email.onBlur}
            ref={email.ref}
            type="email"
          >
            {errors.email && (
              <div className="text-red-500">{errors.email.message}</div>
            )}
          </InputComponent>

          <InputComponent
            name={password.name}
            onChange={password.onChange}
            onBlur={password.onBlur}
            ref={password.ref}
            type="password"
          >
            {errors.password && (
              <div className="text-red-500">{errors.password.message}</div>
            )}
          </InputComponent>
          <ButtonComponent onClick={handleSubmit(onSubmit)}>
            Login
          </ButtonComponent>
        </div>
      </form>
    </Layout>
  );
}

export default login;
