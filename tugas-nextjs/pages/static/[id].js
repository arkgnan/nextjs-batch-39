import { useRouter } from "next/router";
import React from "react";
import Layout from "../../widget/Layout";
import axios from "axios";

export async function getStaticPaths() {
  const { data } = await axios(
    `https://backendexample.sanbercloud.com/api/student-scores`
  );
  console.log(data);
  const paths = data.map((mahasiswa) => ({
    params: { id: mahasiswa.id.toString() },
  }));
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const { data } = await axios(
    `https://backendexample.sanbercloud.com/api/student-scores/${params.id}`
  );
  return {
    props: {
      data,
    },
  };
}

function DetailMahasiswa({ data }) {
  console.log(data);
  const handelIndexScore = (score) => {
    if (parseInt(score) >= 80) {
      return "A";
    } else if (parseInt(score) >= 70 && parseInt(score) < 80) {
      return "B";
    } else if (parseInt(score) >= 60 && parseInt(score) < 70) {
      return "C";
    } else if (parseInt(score) >= 50 && parseInt(score) < 60) {
      return "D";
    } else {
      return "E";
    }
  };
  return (
    <Layout title="Detail Mahasiswa">
      <h1 className="block text-xl font-bold border-b py-4">
        ini adalah detail data
      </h1>
      <div className="text-lg">{data.name}</div>
      <ol>
        <li>Mata kuliah : {data.course}</li>
        <li>Nilai kuliah : {data.score}</li>
        <li>Index nilainya : {handelIndexScore(data.score)}</li>
      </ol>
    </Layout>
  );
}

export default DetailMahasiswa;
