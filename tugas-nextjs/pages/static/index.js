import Link from "next/link";
import React, { useState } from "react";
import Layout from "../../widget/Layout";
import axios from "axios";
import { Card } from "flowbite-react";

export async function getStaticProps(context) {
  const { data } = await axios(
    `https://backendexample.sanbercloud.com/api/student-scores`
  );
  return {
    props: {
      data,
    },
  };
}

function index({ data }) {
  return (
    <Layout title="Daftar Mahasiswa">
      <div className="grid grid-cols-4 gap-6">
        {data.length ? (
          data.map((mahasiswa) => {
            return (
              <KartuMahasiswa key={mahasiswa.id} id={mahasiswa.id}>
                {mahasiswa.name}
              </KartuMahasiswa>
            );
          })
        ) : (
          <div className="grid h-screen place-items-center">
            <h1 className="text-2xl text-bold">Data Mahasiswa Not Found</h1>
          </div>
        )}
      </div>
    </Layout>
  );
}

function KartuMahasiswa({ id, children }) {
  return (
    <>
      <Link href={`/static/${id}`}>
        <Card href="#">
          <div className="text-gray-700 border-b py-3">Kartu Mahasiswa</div>
          <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            {children}
          </h5>
          <p className="font-normal text-gray-700 dark:text-gray-400">
            Klik untuk melihat detail
          </p>
        </Card>
      </Link>
    </>
  );
}

export default index;
