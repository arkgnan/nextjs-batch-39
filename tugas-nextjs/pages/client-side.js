import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import Layout from "../widget/Layout";
import { ButtonComponent } from "../components/ButtonComponent";
import { InputComponent } from "../components/InputComponent";
import {
  QueryClient,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";
import { dehydrate } from "react-query/hydration";
import { useForm } from "react-hook-form";

const handelIndexScore = (score) => {
  if (parseInt(score) >= 80) {
    return "A";
  } else if (parseInt(score) >= 70 && parseInt(score) < 80) {
    return "B";
  } else if (parseInt(score) >= 60 && parseInt(score) < 70) {
    return "C";
  } else if (parseInt(score) >= 50 && parseInt(score) < 60) {
    return "D";
  } else {
    return "E";
  }
};

const getMahasiswa = async () => {
  const { data } = await axios(
    `https://backendexample.sanbercloud.com/api/student-scores`
  ).catch(function (error) {
    if (error.response) {
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
      throw new Error(error.response.data);
    }
  });
  return data;
};

// NextJS SSR with react query can use Initial Data or Hydrate
// Initial Data if we want to use data for single component
// Hydrate is more like state management

// SSR Initial Data Example
export async function getStaticProps() {
  const initialMahasiswa = await getMahasiswa();
  return {
    props: {
      initialMahasiswa,
    },
  };
}

// SSR Hydrate Example
// export async function getStaticProps() {
//   const queryClient = new QueryClient();
//   await queryClient.prefetchQuery(["market", 1], () => getMahasiswa());
//   return {
//     props: {
//       dehydratedState: dehydrate(queryClient),
//     },
//   };
// }

const formatDate = (date) => {
  return new Date(date).toLocaleDateString("id-ID");
};

const submitMahasiswa = async (form) => {
  const { data } = await axios
    .post(`https://backendexample.sanbercloud.com/api/student-scores`, form)
    .catch(function (error) {
      if (error.response) {
        throw new Error(error.response.data);
      }
    });
  return data;
};

export default function ClientSide({ initialMahasiswa }) {
  const queryClient = useQueryClient();
  const [errorMessage, setErrorMessage] = useState("");
  const [page, setPage] = useState(1);
  const nextPage = () => {
    setPage((old) => old + 1);
  };
  const prevPage = () => {
    setPage((old) => old - 1);
  };
  const { data, isError, isLoading, isFetching, isSuccess } = useQuery(
    "get-mahasiswa", // to make data not replace when change the page
    getMahasiswa,
    {
      staleTime: 15000, // when move tab in period time
      refetchInterval: 15000, // auto refetch
      initialData: initialMahasiswa,
    }
  );
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
    clearErrors,
  } = useForm();
  const name = register("name", { required: "Name is required" });
  const score = register("score", { required: "Score is required" });
  const course = register("course", { required: "Course is required" });

  const mutation = useMutation(submitMahasiswa, {
    onMutate: async (newMahasiswa) => {
      // mutation in-progress
      // use for : spinner, disable form, etc
      console.log("onMutate");
      // optimistic update
      // cancel any outgoing refetch
      await queryClient.cancelQueries("get-mahasiswa");
      // snapshot the previouse value
      const prevMahasiswa = queryClient.getQueryData("get-mahasiswa");

      // optimistically update new value
      if (prevMahasiswa) {
        newMahasiswa = {
          ...newMahasiswa,
          created_at: new Date().toISOString(),
        };
        const finalMahasiswa = [...prevMahasiswa, newMahasiswa];
        queryClient.setQueryData("get-mahasiswa", finalMahasiswa);
      }
      return prevMahasiswa;
    },
    onSettled: async (data, error) => {
      console.log("onSettled");
      // await queryClient.invalidateQueries("get-mahasiswa");
      // mutation done -> success or error
      // Some unmounting action.
      // eg: if you have a form in a popup or modal,
      // call your close modal method here.
      // onSettled will trigger once the mutation is done either it
      // succeeds or errors out.
    },
    onError: async (error, variables, context) => {
      console.log("onError", error);
      setErrorMessage(error.message);
      if (context?.prevMahasiswa) {
        queryClient.setQueryData("get-mahasiswa", context.prevMahasiswa);
      }
    },
    onSuccess: async (data, variables, context) => {
      console.log("onSuccess");
      await queryClient.invalidateQueries("get-mahasiswa");
      await queryClient.refetchQueries("get-mahasiswa");
      setErrorMessage("");
      reset();
      clearErrors();
    },
  });

  const onSubmit = async (data) => {
    await mutation.mutate(data);
  };
  return (
    <Layout title="CSR with React Query">
      {isFetching && (
        <div role="status" className="flex justify-center">
          <svg
            className="inline mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-purple-600"
            viewBox="0 0 100 101"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
              fill="currentColor"
            />
            <path
              d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
              fill="currentFill"
            />
          </svg>
          <span className="sr-only">Loading...</span>
        </div>
      )}
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="py-3 px-6">
              Nama
            </th>
            <th scope="col" className="py-3 px-6">
              Mata Kuliah
            </th>
            <th scope="col" className="py-3 px-6">
              Nilai
            </th>
            <th scope="col" className="py-3 px-6">
              Index Nilai
            </th>
            <th scope="col" className="py-3 px-6">
              Tanggal
            </th>
          </tr>
        </thead>
        <tbody>
          {isError && (
            <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
              <th
                scope="row"
                className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                colSpan={4}
              >
                There was an error processing your request
              </th>
            </tr>
          )}
          {isSuccess &&
            data?.map((mhs, i) => {
              return (
                <tr
                  key={i}
                  className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <th
                    scope="row"
                    className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {mhs.name}
                  </th>
                  <td className="py-4 px-6">{mhs.course}</td>
                  <td className="py-4 px-6">{mhs.score}</td>
                  <td className="py-4 px-6">{handelIndexScore(mhs.score)}</td>
                  <td className="py-4 px-6">{formatDate(mhs.created_at)}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
      <div className="grid grid-cols-3 gap-6 mt-10">
        <button
          className="border rounded hover:bg-gray-500"
          onClick={prevPage}
          disabled={page === 1 ? true : false}
        >
          Prev
        </button>
        <div className="text-center">Page : {page}</div>
        <button className="border rounded hover:bg-gray-500" onClick={nextPage}>
          Next
        </button>
      </div>
      <div className="overflow-x-auto relative">
        <form className="mt-12 p-1">
          <InputComponent
            placeholder="input name"
            name={name.name}
            onChange={name.onChange}
            onBlur={name.onBlur}
            ref={name.ref}
          >
            {errors.name && (
              <div className="text-red-500">{errors.name.message}</div>
            )}
          </InputComponent>

          <InputComponent
            placeholder="input course"
            name={course.name}
            onChange={course.onChange}
            onBlur={course.onBlur}
            ref={course.ref}
          >
            {errors.course && (
              <div className="text-red-500">{errors.course.message}</div>
            )}
          </InputComponent>
          <InputComponent
            type="number"
            placeholder="input score"
            name={score.name}
            onChange={score.onChange}
            onBlur={score.onBlur}
            ref={score.ref}
          >
            {errors.score && (
              <div className="text-red-500">{errors.score.message}</div>
            )}
          </InputComponent>
          <ButtonComponent onClick={handleSubmit(onSubmit)}>
            Submit
          </ButtonComponent>
        </form>
      </div>
    </Layout>
  );
}
