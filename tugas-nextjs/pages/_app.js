import "../styles/globals.css";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { Hydrate } from "react-query/hydration";
import { GlobalProvider } from "../context/GlobalContext";
const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      {/* <Hydrate state={pageProps.dehydatedState}> */}
      <GlobalProvider>
        <Component {...pageProps} />
      </GlobalProvider>
      {/* </Hydrate> */}
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default MyApp;
