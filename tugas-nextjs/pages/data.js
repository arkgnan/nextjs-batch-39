import Link from "next/link";
import React from "react";
import Layout from "../widget/Layout";

function data(props) {
  return (
    <Layout title="Data Fetching SSG" className="p-0">
      <div className="grid grid-cols-2 inset-0">
        <div className="bg-red-600 grid w-full h-screen place-items-center">
          <Link href="/static">
            <a
              href="#"
              className="text-white hover:text-gray-300 hover:underline"
            >
              Static
            </a>
          </Link>
        </div>
        <div className="bg-blue-600 grid w-full h-screen place-items-center">
          <Link href="/server-side">
            <a
              href="#"
              className="text-white hover:text-gray-300 hover:underline"
            >
              Server
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  );
}

export default data;
