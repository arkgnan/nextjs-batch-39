import Layout from "../widget/Layout";

export default function Peserta() {
  return (
    <Layout title="Peserta">
      <div className="p-8">
        <h1>Data Peserta Sanbercode Bootcamp Nextjs</h1>
        <ol>
          <li>
            <strong>Nama:</strong> Dedi Ananto
          </li>
          <li>
            <strong>Email:</strong> dedi.ananto@gmail.com
          </li>
          <li>
            <strong>Sistem Operasi yang digunakan:</strong> Windows 11
          </li>
          <li>
            <strong>Akun Gitlab:</strong> arkgnan
          </li>
          <li>
            <strong>Akun Telegram:</strong> dediananto
          </li>
          <li>
            <strong>Tanggal lahir:</strong> 29-12-2022
          </li>
          <li>
            <strong>Pengalaman kerja:</strong> -
          </li>
        </ol>
      </div>
    </Layout>
  );
}
