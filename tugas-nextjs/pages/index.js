import Link from "next/link";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import Layout from "../widget/Layout";

export default function Home() {
  return (
    <Layout title="Home">
      <main className="md:w-2/3 w-full mx-auto border rounded-xl shadow p-8 mt-8">
        <div className="relative w-64 h-32">
          <Image
            src="/logo.png"
            layout="fill"
            objectFit="contain"
            alt="sanbercode logo"
          />
        </div>
        <h1 className="text-3xl font-bold">Article Post</h1>

        <div className="mt-4">
          <div className="border-b mb-8 py-4">
            <h2 className="text-lg font-bold">Lorem Post 1</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur.
            </p>
          </div>
          <div className="border-b mb-8 py-4">
            <h2 className="text-lg font-bold">Lorem Post 2</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur.
            </p>
          </div>
        </div>
        <Link href="/peserta">
          <a
            href="#"
            className="block w-full rounded-lg bg-fuchsia-600 text-white hover:bg-fuchsia-700 text-center py-2 px-3"
          >
            Lihat Data Peserta
          </a>
        </Link>
      </main>
    </Layout>
  );
}
