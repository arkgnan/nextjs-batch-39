import axios from "axios";
import clsx from "clsx";
import React, { useContext, useEffect, useState } from "react";
import Layout from "../widget/Layout";
import { TableComponent } from "../components/TableComponent";
import { InputComponent } from "../components/InputComponent";
import { GlobalContext } from "../context/GlobalContext";

const handelIndexScore = (score) => {
  if (parseInt(score) >= 80) {
    return "A";
  } else if (parseInt(score) >= 70 && parseInt(score) < 80) {
    return "B";
  } else if (parseInt(score) >= 60 && parseInt(score) < 70) {
    return "C";
  } else if (parseInt(score) >= 50 && parseInt(score) < 60) {
    return "D";
  } else {
    return "E";
  }
};

const convertDataMahasiswa = (data) => {
  var mahasiswa = [];
  if (data.length !== 0) {
    data.forEach((key, index) => {
      if (key.name != null) {
        mahasiswa.push({
          id: key.id,
          no: index + 1,
          nama: key.name,
          mata_kuliah: key.course,
          nilai: key.score,
          index_nilai: handelIndexScore(key.score),
        });
      }
    });
  }
  return mahasiswa;
};

export async function getServerSideProps() {
  const { data } = await axios(
    `https://backendexample.sanbercloud.com/api/student-scores`
  );
  const mahasiswa = convertDataMahasiswa(data);

  return {
    props: {
      mahasiswa,
    },
  };
}
export default function ServerSide(props) {
  const [dataMahasiswa, setDataMahasiswa] = useState(props.mahasiswa);
  const { state, handleFunction } = useContext(GlobalContext);
  let {
    input,
    setInput,
    fetchStatus,
    setFetchStatus,
    currentId,
    setCurrentId,
  } = state;

  let { handleChange, handleSubmit, handleDelete, handleEdit } = handleFunction;
  const tableColumn = ["no", "nama", "mata_kuliah", "nilai", "index_nilai"];
  const fetchData = async () => {
    const { data } = await axios.get(
      `https://backendexample.sanbercloud.com/api/student-scores`
    );
    const mahasiswa = convertDataMahasiswa(data);

    setDataMahasiswa(mahasiswa);
  };

  useEffect(() => {
    if (fetchStatus) {
      fetchData();
      setFetchStatus(false);
    }
  }, [fetchStatus, setFetchStatus]);

  return (
    <Layout title="Server Side">
      {dataMahasiswa.length !== 0 && (
        <TableComponent
          column={tableColumn}
          data={dataMahasiswa}
          actionDelete={true}
          actionEdit={true}
          handleDelete={handleDelete}
          handleEdit={handleEdit}
        />
      )}
      <form className="mt-12" onSubmit={handleSubmit}>
        <InputComponent
          name="name"
          placeholder="input name"
          value={input.name}
          onChange={handleChange}
        />
        <InputComponent
          name="course"
          placeholder="input course"
          value={input.course}
          onChange={handleChange}
        />
        <InputComponent
          name="score"
          type="number"
          placeholder="input score"
          value={input.score}
          onChange={handleChange}
        />
        <Button>Submit</Button>
      </form>
    </Layout>
  );
}

function Input(props) {
  const { type = "text", name, placeholder } = props;
  return (
    <div className="mb-6">
      <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
        {name}
      </label>
      <input
        className="bg-gray-50 border border-gray-300 text-gray-900
                    text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
        type={type}
        placeholder={placeholder}
        name={name}
        {...props}
      />
    </div>
  );
}

function Button(props) {
  const {
    type = "submit",
    children,
    className = "bg-purple-700 hover:bg-purple-800",
  } = props;
  return (
    <button
      {...props}
      type={type}
      className={clsx(
        className,
        "text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
      )}
    >
      {children}
    </button>
  );
}
