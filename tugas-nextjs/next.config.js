/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["cdn.pixabay.com"],
  },
  async rewrites() {
    return [
      {
        source: "/login",
        destination: "/auth/login",
      },
      {
        source: "/logout",
        destination: "/auth/logout",
      },
      {
        source: "/home",
        destination: "/",
      },
    ];
  },
  env: {
    appName: "eCommerce Sanbercode",
  },
};

module.exports = nextConfig;
