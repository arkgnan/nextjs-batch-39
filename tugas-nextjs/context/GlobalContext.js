import { createContext, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  const [input, setInput] = useState({
    name: "",
    score: "",
    course: "",
  });

  const [fetchStatus, setFetchStatus] = useState(false);
  const [currentId, setCurrentId] = useState(-1);

  const handleChange = (event) =>
    setInput({ ...input, [event.target.name]: event.target.value });
  const handleEdit = (id) => {
    axios
      .get(`https://backendexample.sanbercloud.com/api/student-scores/${id}`)
      .then((res) => {
        console.log(res);
        setCurrentId(res.data.id);

        setInput({
          name: res.data.name,
          course: res.data.course,
          score: res.data.score,
        });
        console.log("id", currentId);
      });
  };

  const handleDelete = (id) => {
    axios
      .delete(`https://backendexample.sanbercloud.com/api/student-scores/${id}`)
      .then((res) => {
        console.log(res);
        setFetchStatus(true);
      });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let { name, course, score } = input;

    if (currentId === -1) {
      axios
        .post(`https://backendexample.sanbercloud.com/api/student-scores`, {
          name,
          course,
          score,
        })
        .then((res) => {
          setFetchStatus(true);
        });
    } else {
      axios
        .put(
          `https://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
          { name, course, score }
        )
        .then((res) => {
          setFetchStatus(true);
        });
    }

    setInput({
      name: "",
      course: "",
      score: "",
    });

    setCurrentId(-1);
  };

  let state = {
    input,
    setInput,
    fetchStatus,
    setFetchStatus,
    currentId,
    setCurrentId,
  };

  let handleFunction = {
    handleChange,
    handleSubmit,
    handleDelete,
    handleEdit,
  };

  return (
    <GlobalContext.Provider value={{ state, handleFunction }}>
      {props.children}
    </GlobalContext.Provider>
  );
};
