import { forwardRef } from "react";

export const InputComponent = forwardRef(
  (
    { type = "text", name, onChange, onBlur, placeholder, children, ...props },
    ref
  ) => {
    return (
      <div className="mb-6">
        <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
          {name}
        </label>
        <input
          ref={ref}
          className="bg-gray-50 border border-gray-300 text-gray-900
                    text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          type={type}
          placeholder={placeholder}
          name={name}
          onChange={onChange}
          onBlur={onBlur}
          {...props}
        />
        <div className="mt-1">{children}</div>
      </div>
    );
  }
);
