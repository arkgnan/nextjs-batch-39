import clsx from "clsx";
export const ButtonComponent = (props) => {
  const {
    type = "submit",
    children,
    className = "bg-purple-700 hover:bg-purple-800",
  } = props;
  return (
    <button
      {...props}
      type={type}
      className={clsx(
        className,
        "text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
      )}
    >
      {children}
    </button>
  );
};
