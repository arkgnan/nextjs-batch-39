import { Navbar } from "flowbite-react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

export const Navigation = ({ menu }) => {
  const router = useRouter();
  return (
    <>
      <Navbar fluid={true} rounded={true}>
        <Link href="/">
          <a href="#" className="flex item-center">
            <Image
              src="/logo.png"
              className="mr-3 h-6 sm:h-9"
              alt="Flowbite Logo"
              width={150}
              height={50}
            />
            <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white border-l-2 border-fuchsia-700 pl-3">
              Nextjs
            </span>
          </a>
        </Link>
        <Navbar.Toggle />
        <Navbar.Collapse>
          {menu.map((item, i) => {
            return (
              <Link href={item === "home" ? "/" : `/${item}`} key={i}>
                <a
                  href="#"
                  className={
                    router.pathname ===
                    (`/${item}` === "/home" ? "/" : `/${item}`)
                      ? `link-active`
                      : `link-note-active`
                  }
                >
                  {item}
                </a>
              </Link>
            );
          })}
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};
