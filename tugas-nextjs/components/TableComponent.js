import { Table } from "flowbite-react";

export const TableComponent = ({
  column,
  data,
  actionDelete,
  actionEdit,
  handleDelete,
  handleEdit,
}) => {
  return (
    <>
      <Table striped={true} hoverable={true}>
        <Table.Head>
          {column.map((name, index) => {
            return (
              <Table.HeadCell key={index} className="bg-purple-700 text-white">
                {name.replace("_", " ")}
              </Table.HeadCell>
            );
          })}
          {(actionDelete || actionEdit) && (
            <Table.HeadCell className="bg-purple-700 text-white text-center">
              Action
            </Table.HeadCell>
          )}
        </Table.Head>
        <Table.Body className="divide-y">
          {data.map((mhs, index) => {
            return (
              <Table.Row
                key={index}
                className="bg-white dark:border-gray-700 dark:bg-gray-800"
              >
                {column.map((name, i) => {
                  if (i == 0) {
                    return (
                      <Table.Cell
                        key={i}
                        className="whitespace-nowrap font-medium text-gray-900 dark:text-white"
                      >
                        {data[index][name]}
                      </Table.Cell>
                    );
                  } else {
                    return <Table.Cell key={i}>{data[index][name]}</Table.Cell>;
                  }
                })}

                {actionDelete || actionEdit ? (
                  <Table.Cell className="text-center">
                    <div className="flex justify-center items-center gap-x-3">
                      {actionDelete && (
                        <button
                          onClick={() => handleDelete(mhs.id)}
                          className="bg-red-600 rounded text-white hover:bg-red-500 px-2 py-1"
                        >
                          Delete
                        </button>
                      )}
                      {actionEdit && (
                        <button
                          onClick={() => handleEdit(mhs.id)}
                          className="bg-blue-600 rounded text-white hover:bg-blue-500 px-2 py-1"
                        >
                          Edit
                        </button>
                      )}
                    </div>
                  </Table.Cell>
                ) : null}
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    </>
  );
};
