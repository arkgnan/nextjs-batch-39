/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["fitinline.com"],
  },
  async rewrites() {
    return [
      {
        source: "/login",
        destination: "/auth/user-login",
      },
      {
        source: "/admin",
        destination: "/auth/admin-login",
      },
      {
        source: "/cart",
        destination: "/user/cart",
      },
      {
        source: "/transaction",
        destination: "/user/transaction",
      },
      {
        source: "/register",
        destination: "/auth/user-register",
      },
      {
        source: "/dashboard",
        destination: "/admin/dashboard",
      },
      {
        source: "/home",
        destination: "/",
      },
    ];
  },
  env: {
    appName: "eCommerce Sanbercode",
  },
};

module.exports = nextConfig;
