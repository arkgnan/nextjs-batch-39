import axios from "axios";
import Cookies from "js-cookie";
import { createContext, useState } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
  const [user, setUser] = useState(undefined);
  const [checkoutUser, setCheckoutUser] = useState([]);
  const [fetchStatus, setFetchStatus] = useState(false);
  const [fetchCheckoutStatus, setFetchCheckoutStatus] = useState(true);

  const fetchCheckoutUser = async () => {
    try {
      let { data } = await axios(
        `https://service-example.sanbercloud.com/api/checkout-product-user/${user.id}`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_user"),
          },
        }
      );
      data = data.filter((res) => {
        return res.is_transaction === 0;
      });
      setCheckoutUser(data);
    } catch (error) {
      console.log(error);
    }
  };
  let sumTotal = (param) => {
    let filterIsTransaction = param;
    filterIsTransaction.filter((res) => {
      return res.is_transaction === 0;
    });
    let getUnitPrice = filterIsTransaction.map((res) => {
      return res.unit_price;
    });
    let unitPrice = getUnitPrice.map(Number);
    let res = unitPrice.reduce((e, a) => e + a, 0);
    return res;
  };
  const formatCurrency = (value) => {
    return new Intl.NumberFormat().format(value);
  };

  const formatDate = (value) => {
    return new Date(value).toLocaleString("id-ID");
  };

  const removeHTML = (params) => {
    let res = params.replace(/<br>/g, "");
    return res;
  };
  const state = {
    user,
    setUser,
    fetchStatus,
    setFetchStatus,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
    checkoutUser,
    setCheckoutUser,
  };

  const handleFunction = {
    fetchCheckoutUser,
    formatCurrency,
    sumTotal,
    removeHTML,
    formatDate,
  };
  return (
    <GlobalContext.Provider value={{ state, handleFunction }}>
      {props.children}
    </GlobalContext.Provider>
  );
};
