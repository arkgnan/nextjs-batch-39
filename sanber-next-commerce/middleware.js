import { NextRequest, NextResponse } from "next/server";

export function middleware(req, res) {
  if (req.nextUrl.pathname.startsWith("/login")) {
    if (req.cookies.get("token_user") !== undefined) {
      return NextResponse.redirect(new URL("/", req.url));
    }
  }

  if (req.nextUrl.pathname.startsWith("/register")) {
    if (req.cookies.get("token_user") !== undefined) {
      return NextResponse.redirect(new URL("/", req.url));
    }
  }

  if (req.nextUrl.pathname.startsWith("/checkout")) {
    if (req.cookies.get("token_user") === undefined) {
      return NextResponse.redirect(new URL("/login", req.url));
    }
  }

  if (
    req.nextUrl.pathname.startsWith("/dashboard") ||
    req.nextUrl.pathname.startsWith("/admin/")
  ) {
    if (req.cookies.get("token_admin") === undefined) {
      return NextResponse.redirect(new URL("/admin", req.url));
    }
  }
}
