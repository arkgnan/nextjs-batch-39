import React from "react";
import Cookies from "js-cookie";
import axios from "axios";
import { useEffect, useState } from "react";
import CardStats from "../Card/CardStats";

function HeaderStats(props) {
  const [lenghtData, setLengthData] = useState({
    product: 0,
    category: 0,
    transaction: 0,
    checkout: 0,
  });
  const [fetchStatus, setFetchStatus] = useState(false);
  useEffect(() => {
    const getTransaction = async () => {
      const { data } = await axios(
        `https://service-example.sanbercloud.com/api/transaction`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      );
      setLengthData((prevState) => ({
        ...prevState,
        transaction: data.length,
      }));
    };

    const getCheckout = async () => {
      const { data } = await axios(
        `https://service-example.sanbercloud.com/api/checkout`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      );
      setLengthData((prevState) => ({
        ...prevState,
        checkout: data.length,
      }));
    };

    const getProduct = async () => {
      const { data } = await axios(
        `https://service-example.sanbercloud.com/api/product `,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      );
      setLengthData((prevState) => ({
        ...prevState,
        product: data.length,
      }));
    };

    const getCategory = async () => {
      const { data } = await axios(
        `https://service-example.sanbercloud.com/api/category `,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      );
      setLengthData((prevState) => ({
        ...prevState,
        category: data.length,
      }));
    };

    if (fetchStatus == false) {
      getTransaction();
      getCheckout();
      getProduct();
      getCategory();
      setFetchStatus(true);
    }
  }, [lenghtData, setLengthData, fetchStatus, setFetchStatus]);
  return (
    <div>
      <div className="relative bg-slate-800 md:pt-32 pb-32 pt-12">
        <div className="px-4 md:px-10 mx-auto w-full">
          <div>
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Product"
                  statTitle={lenghtData.product}
                  statPercentColor="text-emerald-500"
                  statIconColor="bg-red-500"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M21 11.25v8.25a1.5 1.5 0 01-1.5 1.5H5.25a1.5 1.5 0 01-1.5-1.5v-8.25M12 4.875A2.625 2.625 0 109.375 7.5H12m0-2.625V7.5m0-2.625A2.625 2.625 0 1114.625 7.5H12m0 0V21m-8.625-9.75h18c.621 0 1.125-.504 1.125-1.125v-1.5c0-.621-.504-1.125-1.125-1.125h-18c-.621 0-1.125.504-1.125 1.125v1.5c0 .621.504 1.125 1.125 1.125z"
                    />
                  </svg>
                </CardStats>
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Category"
                  statTitle={lenghtData.category}
                  statPercentColor="text-red-500"
                  statIconColor="bg-orange-500"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M9.568 3H5.25A2.25 2.25 0 003 5.25v4.318c0 .597.237 1.17.659 1.591l9.581 9.581c.699.699 1.78.872 2.607.33a18.095 18.095 0 005.223-5.223c.542-.827.369-1.908-.33-2.607L11.16 3.66A2.25 2.25 0 009.568 3z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6 6h.008v.008H6V6z"
                    />
                  </svg>
                </CardStats>
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Transactions"
                  statTitle={lenghtData.transaction}
                  statPercentColor="text-orange-500"
                  statIconColor="bg-pink-500"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M3.75 3v11.25A2.25 2.25 0 006 16.5h2.25M3.75 3h-1.5m1.5 0h16.5m0 0h1.5m-1.5 0v11.25A2.25 2.25 0 0118 16.5h-2.25m-7.5 0h7.5m-7.5 0l-1 3m8.5-3l1 3m0 0l.5 1.5m-.5-1.5h-9.5m0 0l-.5 1.5m.75-9l3-3 2.148 2.148A12.061 12.061 0 0116.5 7.605"
                    />
                  </svg>
                </CardStats>
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="Checkout"
                  statTitle={lenghtData.checkout}
                  statPercentColor="text-emerald-500"
                  statIconColor="bg-sky-500"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M2.25 18.75a60.07 60.07 0 0115.797 2.101c.727.198 1.453-.342 1.453-1.096V18.75M3.75 4.5v.75A.75.75 0 013 6h-.75m0 0v-.375c0-.621.504-1.125 1.125-1.125H20.25M2.25 6v9m18-10.5v.75c0 .414.336.75.75.75h.75m-1.5-1.5h.375c.621 0 1.125.504 1.125 1.125v9.75c0 .621-.504 1.125-1.125 1.125h-.375m1.5-1.5H21a.75.75 0 00-.75.75v.75m0 0H3.75m0 0h-.375a1.125 1.125 0 01-1.125-1.125V15m1.5 1.5v-.75A.75.75 0 003 15h-.75M15 10.5a3 3 0 11-6 0 3 3 0 016 0zm3 0h.008v.008H18V10.5zm-12 0h.008v.008H6V10.5z"
                    />
                  </svg>
                </CardStats>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderStats;
