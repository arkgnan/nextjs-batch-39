import { useContext } from "react";
import { GlobalContext } from "../../context/GlobalContext";

export default function CartItemComponent({ data }) {
  const { handleFunction } = useContext(GlobalContext);
  const { formatCurrency } = handleFunction;
  return (
    <div className="w-full border-b shadow p-3">
      <div className="flex justify-between items-center">
        <div>
          <div className="font-xs font-bold">{data.product.product_name}</div>
          <div className="flex">
            <div className="font-xs">
              Rp {formatCurrency(data.product.price)}
            </div>
            <div className="font-xs ml-2">x @{data.quantity} pcs</div>
          </div>
        </div>
        <div className="text-right">
          <p className="font-bold">Rp {formatCurrency(data.unit_price)}</p>
        </div>
      </div>
    </div>
  );
}
