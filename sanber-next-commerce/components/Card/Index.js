import axios from "axios";
import Cookies from "js-cookie";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { GlobalContext } from "../../context/GlobalContext";

export const CardComponent = ({ data }) => {
  const { state, handleFunction } = useContext(GlobalContext);
  const { formatCurrency } = handleFunction;
  const {
    user,
    setUser,
    fetchStatus,
    setFetchStatus,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
  } = state;
  const [quantity, setQuantity] = useState(1);
  const [displaySpinner, setDisplaySpinner] = useState(false);
  const router = useRouter();
  // useEffect(() => {
  //   if (Cookies.get("token_user") !== undefined) {
  //     if (user === undefined) {
  //       setUser(JSON.parse(Cookies.get("user")));
  //     }
  //   }
  // }, [user, setUser]);
  const handleChange = (event) => {
    setQuantity(parseInt(event.target.value));
  };
  const handleQuantityPlus = () => {
    setQuantity(quantity + 1);
  };
  const handleQuantityMin = () => {
    quantity > 0 && setQuantity(quantity - 1);
  };
  const handleCheckout = (event) => {
    if (!user) {
      router.push("/login");
    } else {
      setDisplaySpinner(true);
      const postCheckout = async () => {
        const { data } = await axios.post(
          `https://service-example.sanbercloud.com/api/checkout/${user.id}/${event.target.value}`,
          { quantity },
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token_user"),
            },
          }
        );
        console.log(data);
        setDisplaySpinner(false);
        setFetchStatus(true);
        setFetchCheckoutStatus(true);
      };

      postCheckout();
    }
  };
  return (
    <>
      <div
        className="relative border border-gray-100 rounded-lg overflow-hidden"
        style={{ width: "100%" }}
      >
        <div className="relative object-cover w-full ">
          <Image
            src={`/api/imageproxy?url=${encodeURIComponent(data.image_url)}`}
            alt={data.product_name}
            width={100}
            height={100}
            className="w-full h-48 object-cover"
          />
        </div>
        {/* <img className="object-cover w-full h-56 lg:h-72" src={data.image_url} alt="Build Your Own Drone" loading="lazy" /> */}
        <div className="p-6">
          <small>
            <span className="bg-green-100 text-green-800 text-sm font-medium mr-2 px-2.5 py-0.5 rounded-r-lg dark:bg-green-200 dark:text-green-900 truncate">
              {data.category.category_name}
            </span>
          </small>
          <div className="mt-4 truncate">
            <h5>{data.product_name}</h5>
          </div>
          <ul className="mt-5 text-sm font-thin text-gray-500 ">
            <li>Stock : {data.stock}</li>
            <li className="text-lg font-bold">
              Harga : Rp {formatCurrency(data.price)}
            </li>
          </ul>
          {user && (
            <>
              <div className="flex items-center justify-between mt-4 border">
                <button
                  className="h-full px-2 text-black bg-gray-200"
                  onClick={handleQuantityMin}
                >
                  -
                </button>
                <input
                  onChange={handleChange}
                  onFocus={(event) => event.target.select()}
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  className="inline-block w-full h-full text-center focus:outline-none"
                  value={quantity}
                />
                <button
                  className="h-full px-2 text-black bg-gray-200"
                  onClick={handleQuantityPlus}
                >
                  +
                </button>
              </div>
            </>
          )}
          {!displaySpinner && (
            <button
              value={data.id}
              className="block w-full p-4 mt-5 text-sm font-medium text-white bg-blue-500 border rounded-sm"
              type="button"
              onClick={handleCheckout}
            >
              Add to Cart
            </button>
          )}
          {displaySpinner && (
            <button
              className="block w-full p-3.5 mt-5 text-sm font-medium text-white bg-blue-200 border rounded-sm"
              type="button"
            >
              <div role="status" className="flex justify-center">
                <svg
                  aria-hidden="true"
                  className="w-6 h-6 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                  viewBox="0 0 100 101"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                    fill="currentColor"
                  />
                  <path
                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                    fill="currentFill"
                  />
                </svg>
                <span className="sr-only">Loading...</span>
              </div>
            </button>
          )}

          <Link href={`/detail-product/${data.id}`}>
            <button
              className="block w-full p-4 mt-2 text-sm font-medium text-center text-blue-500 bg-white border border-blue-500 rounded-sm"
              type="button"
            >
              Detail Product
            </button>
          </Link>
        </div>
      </div>
    </>
  );
};
