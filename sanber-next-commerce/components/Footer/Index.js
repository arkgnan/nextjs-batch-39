import { Footer } from "flowbite-react";

export const FooterComponent = (props) => {
  return (
    <div className="px-0 lg:px-4">
      <Footer container={true}>
        <Footer.Copyright href="#" by="Flowbite™" year={2022} />
        <Footer.LinkGroup className="gap-x-3 lg:gap-x-6">
          <Footer.Link href="#">About</Footer.Link>
          <Footer.Link href="#">Privacy Policy</Footer.Link>
          <Footer.Link href="#">Licensing</Footer.Link>
          <Footer.Link href="#">Contact</Footer.Link>
        </Footer.LinkGroup>
      </Footer>
    </div>
  );
};
