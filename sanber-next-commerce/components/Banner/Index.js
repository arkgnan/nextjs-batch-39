import { Carousel } from "flowbite-react";
import Image from "next/image";
import banner1 from "../../public/images/banner/banner1.png";
import banner2 from "../../public/images/banner/banner2.png";

export const BannerComponent = (props) => {
  return (
    <>
      <div className="h-32 md:h-60 lg:h-96">
        <Carousel>
          <Image src={banner1} alt="..." placeholder="blur" />
          <Image src={banner2} alt="..." placeholder="blur" />
        </Carousel>
      </div>
    </>
  );
};
