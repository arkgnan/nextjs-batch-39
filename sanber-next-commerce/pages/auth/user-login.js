import React from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useRouter } from "next/router";
import jwt_decode from "jwt-decode";
import Cookies from "js-cookie";
import Link from "next/link";
import AuthLayout from "../../layouts/AuthLayout";

function Login(props) {
  const router = useRouter();
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
    clearErrors,
    setError,
  } = useForm();
  const onSubmit = async (form) => {
    const { data } = await axios
      .post(`https://service-example.sanbercloud.com/api/login`, form)
      .catch(function (error) {
        if (error.response) {
          throw new Error(error.response.data);
        }
      });
    const decoded = jwt_decode(data.token);
    if (decoded.role !== "admin") {
      Cookies.set("token_user", data.token, { expires: 1 });
      Cookies.set("user", JSON.stringify(data.user), { expires: 1 });
    } else {
      throw new Error("You are not valid user");
    }
    reset;
    router.push("/");
  };
  return (
    <AuthLayout title="Login">
      <form
        className="md:w-1/2 lg:w-1/3 xl:w-1/4 p-6 lg:border lg:rounded-lg lg:bg-slate-50"
        onSubmit={handleSubmit(onSubmit)}
      >
        <h1 className="font-bold text-2xl">Login</h1>
        <div className="mt-4">
          <label
            htmlFor="email-address-icon"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
          >
            Email
          </label>
          <div className="relative">
            <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                className="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
              </svg>
            </div>
            <input
              {...register("email", {
                required: "Email is required",
              })}
              type="email"
              id="email-address-icon"
              className="bg-white border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="name@flowbite.com"
            />
          </div>
          {errors.email && (
            <div className="text-red-500 mt-1">{errors.email.message}</div>
          )}
        </div>
        <div className="mt-4">
          <label
            htmlFor="email-address-icon"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
          >
            Password
          </label>
          <div className="relative">
            <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M16.5 10.5V6.75a4.5 4.5 0 10-9 0v3.75m-.75 11.25h10.5a2.25 2.25 0 002.25-2.25v-6.75a2.25 2.25 0 00-2.25-2.25H6.75a2.25 2.25 0 00-2.25 2.25v6.75a2.25 2.25 0 002.25 2.25z"
                />
              </svg>
            </div>
            <input
              {...register("password", {
                required: "Password is required",
              })}
              type="password"
              id="email-address-icon"
              className="bg-white border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="******"
            />
          </div>
          {errors.password && (
            <div className="text-red-500 mt-1">{errors.password.message}</div>
          )}
        </div>
        <div className="mt-6 flex justify-between">
          <label
            htmlFor="terms"
            className="text-sm font-medium text-blue-600 hover:underline dark:text-blue-50"
          >
            <Link href="/">back to home</Link>
          </label>
          <button
            type="submit"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
          >
            Submit
          </button>
        </div>
      </form>
    </AuthLayout>
  );
}

export default Login;
