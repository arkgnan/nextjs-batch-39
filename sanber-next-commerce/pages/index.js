import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { CardComponent } from "../components/Card/Index";
import { GlobalContext } from "../context/GlobalContext";
import MainLayout from "../layouts/MainLayout";
export async function getServerSideProps() {
  try {
    const { data } = await axios(
      `https://service-example.sanbercloud.com/api/product`
    );
    const products = await data;
    return {
      props: {
        products,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}
export default function Home({ products }) {
  const [dataProducts, setDataProducts] = useState(products);
  const [displaySpinner, setDisplaySpinner] = useState(false);
  const [randomIndex, setRandomIndex] = useState(null);
  const [limit, setLimit] = useState(5);
  const { state } = useContext(GlobalContext);
  const { user, setUser, fetchStatus, setFetchStatus } = state;
  const handleCounterFilter = () => {
    setDisplaySpinner(true);
    setLimit(limit + 5);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios(
          `https://service-example.sanbercloud.com/api/product`
        );
        setDataProducts(data);
      } catch (error) {}
    };
    if (fetchStatus) {
      fetchData();
      setFetchStatus(false);
    }
    if (products !== undefined) {
      const filter = products.filter((res) => {
        return res.available === 1;
      });

      const random = Math.floor(Math.random() * filter.length);
      setRandomIndex(random);
    }
  }, [products, fetchStatus, setFetchStatus]);

  try {
    return (
      <MainLayout home>
        <div className="my-6 border-b-2 pb-6">
          <div className="my-6 lg:my-12">
            <h1 className="font-bold text-2xl">Recommended Products</h1>
          </div>
          <div className="grid sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-4">
            {dataProducts
              ?.filter((res, index) => {
                return (
                  res.available === 1 &&
                  index > randomIndex &&
                  index < randomIndex + 3
                );
              })
              .map((product) => {
                return (
                  <CardComponent
                    key={`recommended-${product.id}`}
                    data={product}
                  />
                );
              })}
          </div>
        </div>
        <div className="my-6">
          <div className="my-6 lg:my-12">
            <h1 className="font-bold text-2xl">All Products</h1>
          </div>
          <div className="grid sm:grid-cols-2 lg:grid-cols-4 xl:grid-cols-5 gap-4">
            {dataProducts
              ?.filter((res, index) => {
                return res.available === 1 && index < limit;
              })
              .map((product) => {
                return <CardComponent key={product.id} data={product} />;
              })}
          </div>
          <div className="container mx-auto mt-20 w-full flex justify-center ">
            {!displaySpinner && (
              <button
                type="button"
                onClick={handleCounterFilter}
                className="flex justify-center items-center py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-full border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
              >
                Muat data lainnya{" "}
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                  />
                </svg>
              </button>
            )}
            {displaySpinner && (
              <div role="status">
                <svg
                  aria-hidden="true"
                  className="mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
                  viewBox="0 0 100 101"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                    fill="currentColor"
                  />
                  <path
                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                    fill="currentFill"
                  />
                </svg>
                <span className="sr-only">Loading...</span>
              </div>
            )}
          </div>
        </div>
      </MainLayout>
    );
  } catch (error) {
    return <div className="p-5 text-red-600">Error API connection...</div>;
  }
}
