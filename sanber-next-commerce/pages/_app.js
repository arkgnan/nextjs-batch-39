import { GlobalProvider } from "../context/GlobalContext";
import "../styles/globals.css";
import "../styles/tailwind.css";
import "nprogress/nprogress.css";
import dynamic from "next/dynamic";

const TopProgressBar = dynamic(
  () => {
    return import("../components/ProgressBar/TopProgressBar");
  },
  { ssr: false }
);

function MyApp({ Component, pageProps }) {
  return (
    <GlobalProvider>
      <TopProgressBar />
      <Component {...pageProps} />
    </GlobalProvider>
  );
}

export default MyApp;
