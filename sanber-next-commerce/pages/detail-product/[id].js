import axios from "axios";
import Cookies from "js-cookie";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../context/GlobalContext";
import MainLayout from "../../layouts/MainLayout";

function DetailProduct(props) {
  const router = useRouter();
  const { id } = router.query;
  const { state, handleFunction } = useContext(GlobalContext);
  const { formatCurrency } = handleFunction;
  const {
    user,
    setUser,
    fetchStatus,
    setFetchStatus,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
  } = state;
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const [displaySpinner, setDisplaySpinner] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      try {
        axios
          .get(`https://service-example.sanbercloud.com/api/product/${id}`)
          .then((res) => {
            setProduct(res.data);
            console.log("refetch", product);
          });
      } catch (error) {
        console.log(error);
      }
    };

    // if (Cookies.get("token_user") !== undefined) {
    //   if (user === undefined) {
    //     setUser(JSON.parse(Cookies.get("user")));
    //   }
    // }

    if (fetchStatus) {
      fetchData();
      setFetchStatus(false);
      setFetchCheckoutStatus(true);
    }

    if (id !== undefined) {
      axios
        .get(`https://service-example.sanbercloud.com/api/product/${id}`)
        .then((res) => {
          setProduct(res.data);
          console.log(product);
        });
    }
  }, [id, fetchStatus, setFetchStatus, product, setFetchCheckoutStatus]);
  const handleQuantityPlus = () => {
    setQuantity(quantity + 1);
  };
  const handleQuantityMin = () => {
    quantity > 0 && setQuantity(quantity - 1);
  };
  const handleCheckout = (event) => {
    if (!user) {
      router.push("/login");
    } else {
      setDisplaySpinner(true);
      const postCheckout = async () => {
        console.log(event);
        const { data } = await axios.post(
          `https://service-example.sanbercloud.com/api/checkout/${user.id}/${event.target.value}`,
          { quantity },
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token_user"),
            },
          }
        );
        console.log(data);
        setDisplaySpinner(false);
        setFetchStatus(true);
        setFetchCheckoutStatus(true);
      };
      postCheckout();
    }
  };
  return (
    <MainLayout>
      {product !== null && (
        <div className="container mx-auto mt-6 mb-20">
          <h1 className="text-2xl font-bold mb-6">Detail Product</h1>
          <div className="lg:flex lg:flex-row max-full bg-white shadow-lg rounded-lg overflow-hidden">
            <div className="w-full lg:w-1/3 bg-cover bg-landscape">
              <Image
                src={`/api/imageproxy?url=${encodeURIComponent(
                  product.image_url
                )}`}
                alt={product.product_name}
                width={100}
                height={100}
                className="w-full h-full object-cover"
              />
            </div>
            <div className="w-full lg:w-2/3 p-4 block lg:flex lg:flex-col lg:grow">
              <h1 className="text-gray-900 font-bold text-2xl">
                {product.product_name}
              </h1>
              <p className="mt-2 text-gray-600 text-sm grow">
                {product.description}
              </p>
              <div className="flex item-center justify-between mt-3">
                <div className="grid space-y-1 content-end">
                  <div className="flex item-center">
                    <p>Stock : {product.stock}</p>
                  </div>
                  <h1 className="text-gray-700 font-bold text-xl">
                    Rp {formatCurrency(product.price)}
                  </h1>
                </div>
                <div className="grid space-y-1 content-end w-40">
                  {user && (
                    <>
                      <div className="flex items-center justify-between mt-4 border">
                        <button
                          className="h-full px-2 text-black bg-gray-200"
                          onClick={handleQuantityMin}
                        >
                          -
                        </button>
                        <input
                          className="inline-block w-full h-full text-center focus:outline-none"
                          placeholder={quantity}
                        />
                        <button
                          className="h-full px-2 text-black bg-gray-200"
                          onClick={handleQuantityPlus}
                        >
                          +
                        </button>
                      </div>
                    </>
                  )}
                  {!displaySpinner && (
                    <button
                      value={product.id}
                      className="px-3 py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded"
                      onClick={handleCheckout}
                    >
                      Add to Card
                    </button>
                  )}
                  {displaySpinner && (
                    <button className="px-3 py-2 bg-gray-400 text-white text-xs font-bold uppercase rounded block w-full">
                      <div role="status" className=" block w-full">
                        <svg
                          className="inline w-4 h-4 text-gray-200 animate-spin dark:text-gray-600 fill-gray-600 dark:fill-gray-300"
                          viewBox="0 0 100 101"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                            fill="currentColor"
                          />
                          <path
                            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                            fill="currentFill"
                          />
                        </svg>
                        <span className="sr-only">Loading...</span>
                      </div>
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {product === null && (
        <div className="container mx-auto my-16 w-full flex justify-center">
          <div role="status">
            <svg
              aria-hidden="true"
              className="mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
              viewBox="0 0 100 101"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                fill="currentColor"
              />
              <path
                d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                fill="currentFill"
              />
            </svg>
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      )}
    </MainLayout>
  );
}

export default DetailProduct;
