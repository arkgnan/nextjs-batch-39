import axios from "axios";
import React, { useEffect, useContext, useState } from "react";
import Cookies from "js-cookie";
import { GlobalContext } from "../../../context/GlobalContext";
import MainLayout from "../../../layouts/MainLayout";

function Transaction(props) {
  const { state, handleFunction } = useContext(GlobalContext);
  const { formatCurrency } = handleFunction;
  const { user, setUser } = state;
  const [fetchTransactionStatus, setFetchTransactionStatus] = useState(true);
  const [transactionData, setTransactionData] = useState([]);
  useEffect(() => {
    if (Cookies.get("token_user") !== undefined) {
      if (user === undefined) {
        setUser(JSON.parse(Cookies.get("user")));
      }
    }
    if (user !== undefined) {
      if (fetchTransactionStatus) {
        axios
          .get(
            `https://service-example.sanbercloud.com/api/transaction-user/${user.id}`,
            {
              headers: {
                Authorization: "Bearer " + Cookies.get("token_user"),
              },
            }
          )
          .then((res) => {
            console.log(res.data);
            setTransactionData(res.data);
          });
        setFetchTransactionStatus(false);
      }
    }
  }, [user, setUser, fetchTransactionStatus, setFetchTransactionStatus]);

  const handleCompleteTransaction = (event) => {
    console.log(event.target.value);
    axios
      .post(
        `https://service-example.sanbercloud.com/api/transaction-completed/${event.target.value}/${user.id}`,
        {},
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_user"),
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        setFetchTransactionStatus(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleDeleteTransaction = (event) => {
    axios
      .delete(
        `https://service-example.sanbercloud.com/api/transaction/${event.target.value}`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_user"),
          },
        }
      )
      .then((res) => {
        console.log(res.data);
        setFetchTransactionStatus(true);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <MainLayout title="History Transaction">
      <div className="container mx-auto mt-10 mb-5">
        <h1 className="text-2xl font-bold">History Transaction</h1>
      </div>
      {transactionData?.map((data) => {
        return (
          <div
            key={data.id}
            className="mb-4 bg-white shadow-lg rounded-lg border p-4"
          >
            <div className="flex justify-between items-center">
              <div>
                <div className="font-xs font-bold">{data.status}</div>
                <div className="">
                  <div className="font-xs">{data.transaction_code}</div>
                  <div className="font-xs">
                    Total Rp {formatCurrency(data.total)}
                  </div>
                </div>
              </div>
              <div className="text-right">
                <div className="grid space-y-1">
                  {data.status == "Transaksi terdaftar" && (
                    <button
                      onClick={handleCompleteTransaction}
                      value={data.id}
                      className="px-4 py-2 bg-sky-600 hover:bg-sky-400 rounded-lg text-white"
                    >
                      Transaksi Selesai
                    </button>
                  )}
                  {data.status == "Transaksi selesai" && (
                    <button
                      onClick={handleDeleteTransaction}
                      value={data.id}
                      className="px-4 py-2 bg-rose-600 hover:bg-rose-700 rounded-lg text-white"
                    >
                      Hapus Transaksi
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </MainLayout>
  );
}

export default Transaction;
