import axios from "axios";
import Cookies from "js-cookie";
import Image from "next/image";
import React, { useContext, useEffect, useState, useRef } from "react";
import CartItemComponent from "../../../components/Card/CartItem";
import { GlobalContext } from "../../../context/GlobalContext";
import MainLayout from "../../../layouts/MainLayout";
import emailjs from "@emailjs/browser";
export async function getServerSideProps() {
  try {
    const { data } = await axios(
      `http://service-example.sanbercloud.com/api/bank`
    );
    const banks = await data;

    return {
      props: {
        banks,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}
export default function Cart({ banks }) {
  const { state, handleFunction } = useContext(GlobalContext);
  const { sumTotal, formatCurrency, removeHTML } = handleFunction;
  const {
    user,
    setUser,
    checkoutUser,
    setCheckoutUser,
    fetchCheckoutStatus,
    setFetchCheckoutStatus,
  } = state;
  const [selectedBank, setSelectedBank] = useState(-1);
  const [disableBtn, setDisableBtn] = useState(true);
  const [bankName, setBankName] = useState("");
  const [input, setInput] = useState({
    message: "",
  });
  let form = useRef();

  const handleCheckout = (event) => {
    event.preventDefault();
    setDisableBtn(true);
    axios
      .post(
        `https://service-example.sanbercloud.com/api/transaction/${user.id}`,
        { id_bank: selectedBank },
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_user"),
          },
        }
      )
      .then((res) => {
        emailjs
          .sendForm(
            "service_lpsrsos",
            "template_4y2u3p4",
            form.current,
            "FVg1gXHjeBYNIqBx-"
          )
          .then(
            (result) => {
              console.log(result.text);
            },
            (error) => {
              console.log(error.text);
            }
          );
        setDisableBtn(false);
        setCheckoutUser([]);
        setFetchCheckoutStatus(true);
        location.href = "/";
      })
      .catch((error) => {
        alert(error);
      });
  };

  useEffect(() => {
    if (user !== undefined) {
      if (checkoutUser.length == 0) {
        const fetchCheckoutUser = async () => {
          const { data } = await axios(
            `https://service-example.sanbercloud.com/api/checkout-product-user/${user.id}`,
            {
              headers: {
                Authorization: "Bearer " + Cookies.get("token_user"),
              },
            }
          );
          setCheckoutUser(data);
          let productName = checkoutUser
            .filter((res) => {
              return res.is_transaction === 0;
            })
            .map((res) => {
              return res.product.product_name;
            });
          setInput({
            ...input,
            message: `Anda memiliki transaksi di website.id dengan nama product : <br>\n- ${productName.join(
              "<br>\n- "
            )} <br><br>\n\nDengan Total Pembayaran : Rp${formatCurrency(
              sumTotal(checkoutUser)
            )}<br>\nSilahkan lakukan pembayaran segera ke bank ${
              selectedBank !== -1 ? bankName : "yang dipilih"
            }`,
          });
        };
        fetchCheckoutUser();
      }
    }
  }, [
    user,
    setUser,
    checkoutUser,
    setCheckoutUser,
    bankName,
    formatCurrency,
    selectedBank,
    sumTotal,
    input,
  ]);
  try {
    return (
      <MainLayout title="My Cart">
        <div className="container mx-auto mt-10 mb-5">
          <h1 className="text-2xl font-bold">Cart Page</h1>
          <p>
            You have <strong>{checkoutUser.length}</strong> items in your cart
          </p>
        </div>
        <div className="mb-10 bg-white shadow-lg rounded-lg border p-4">
          {checkoutUser.length > 0 &&
            checkoutUser
              .filter((data) => {
                return data.is_transaction === 0;
              })
              .map((data, index) => {
                return (
                  <div
                    key={index}
                    className="lg:flex lg:flex-row max-full overflow-hidden  mb-3"
                  >
                    <div className="w-20 h-20">
                      <Image
                        src={`/api/imageproxy?url=${encodeURIComponent(
                          data.product.image_url
                        )}`}
                        alt={data.product.product_name}
                        width={100}
                        height={100}
                        className="w-full h-full object-cover mb-3"
                      />
                    </div>

                    <CartItemComponent key={data.id} data={data} />
                  </div>
                );
              })}
          <form ref={form} onSubmit={handleCheckout}>
            <div className="relative mb-6">
              <label
                htmlFor="bank"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
              >
                Pilih Bank transaksi Anda
              </label>
              <select
                id="bank"
                name="bank"
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                value={selectedBank}
                onChange={(e) => {
                  setSelectedBank(e.target.value);
                  setBankName(e.target.options[e.target.selectedIndex].text);
                  console.log(bankName);
                  if (e.target.value == -1) {
                    setDisableBtn(true);
                  } else {
                    setDisableBtn(false);
                  }
                }}
              >
                <option defaultValue={-1} value={-1}>
                  Choose a Bank
                </option>
                {banks?.map((bank) => {
                  return (
                    <option value={bank.id} key={bank.id}>
                      {bank.bank_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="hidden">
              <div>
                <label
                  htmlFor="name"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
                >
                  Data Anda
                </label>
                <div className="relative mb-6">
                  <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 20 20"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="w-5 h-5"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                      />
                    </svg>
                  </div>
                  <input
                    type="text"
                    id="name"
                    name="user_name"
                    className="bg-gray-200 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={user !== undefined && user.name}
                    readOnly={true}
                  />
                </div>
              </div>
              <div>
                <div className="relative mb-6">
                  <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5 text-gray-500 dark:text-gray-400"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                      <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                    </svg>
                  </div>
                  <input
                    type="text"
                    id="email"
                    name="user_email"
                    className="bg-gray-200 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={user !== undefined && user.email}
                    // value="dedi.ananto@gmail.com"
                    readOnly={true}
                  />
                </div>
              </div>
              <div>
                <label
                  htmlFor="message"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
                >
                  Message
                </label>
                <textarea
                  id="message"
                  name="message"
                  rows={4}
                  className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Your message..."
                  defaultValue={removeHTML(input.message)}
                />
                <input type="hidden" value="website.id" name="website_name" />
              </div>
            </div>
            <div className="lg:flex lg:flex-row max-full overflow-hidden">
              <div className="w-full lg:w-2/3block lg:flex lg:flex-col lg:grow">
                <p className="text-gray-800">
                  Press <strong>checkout</strong> button if you want to process
                  the order
                </p>
                <div className="flex item-center justify-between">
                  <h1 className="text-gray-700 font-bold text-xl">
                    Total : Rp{" "}
                    {checkoutUser.length !== 0 &&
                      formatCurrency(sumTotal(checkoutUser))}
                  </h1>
                  <button
                    type="submit"
                    disabled={disableBtn}
                    className={`${
                      disableBtn ? "bg-gray-500" : "bg-gray-800"
                    } px-3 py-2  text-white text-xs font-bold uppercase rounded`}
                  >
                    Checkout
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </MainLayout>
    );
  } catch (error) {
    return <div className="p-5 text-red-600">Error API connection...</div>;
  }
}
