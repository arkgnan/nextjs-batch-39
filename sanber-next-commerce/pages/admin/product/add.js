import React, { useState } from "react";
import { useForm } from "react-hook-form";
import Cookies from "js-cookie";
import axios from "axios";
import { useRouter } from "next/router";
import AdminLayout from "../../../layouts/AdminLayout";
export async function getServerSideProps(context) {
  const adminToken = context.req.cookies["token_admin"];

  try {
    const getCategory = await axios(
      `https://service-example.sanbercloud.com/api/category`
    );

    const category = await getCategory.data;
    return {
      props: {
        category,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}
export default function Add({ category }) {
  const router = useRouter();
  const [formProduct, setFormProduct] = useState({
    product_name: "",
    id_category: -1,
    description: "",
    image_url: "",
    stock: 0,
    rating: 0,
    price: 0,
    available: 1,
  });
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
    clearErrors,
  } = useForm();
  const onSubmit = async (form) => {
    console.log(form);
    const { data } = await axios
      .post(`https://service-example.sanbercloud.com/api/product`, form, {
        headers: {
          Authorization: "Bearer " + Cookies.get("token_admin"),
        },
      })
      .catch(function (error) {
        if (error.response) {
          throw new Error(error.response.data);
        }
      });
    router.push("/admin/product");
  };

  return (
    <AdminLayout title="Add New Product">
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-slate-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-slate-700 text-xl font-bold">
              Add New Product
            </h6>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          <form className="px-4 py-6" onSubmit={handleSubmit(onSubmit)}>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Category
              </label>
              <select
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                name="id_category"
                value={formProduct.id_category}
                {...register("id_category")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    id_category: e.target.value,
                  }))
                }
              >
                <option defaultValue={-1} value={-1}>
                  Choose category
                </option>
                {category?.map((data) => {
                  return (
                    <option value={data.id} key={data.id}>
                      {data.category_name}
                    </option>
                  );
                })}
              </select>

              {errors.id_category && (
                <div className="mt-1">
                  <div className="text-red-500">
                    {errors.id_category.message}
                  </div>
                </div>
              )}
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Product Name
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="text"
                placeholder="Product name"
                name="product_name"
                value={formProduct.product_name}
                {...register("product_name")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    product_name: e.target.value,
                  }))
                }
              />

              {errors.product_name && (
                <div className="mt-1">
                  <div className="text-red-500">
                    {errors.product_name.message}
                  </div>
                </div>
              )}
            </div>
            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Deskripsi
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="text"
                placeholder="Write detail or specification product"
                name="description"
                value={formProduct.description}
                {...register("description")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    description: e.target.value,
                  }))
                }
              />

              {errors.description && (
                <div className="mt-1">
                  <div className="text-red-500">
                    {errors.description.message}
                  </div>
                </div>
              )}
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Image Url
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="text"
                placeholder="https://gambar.com/file.jpg"
                name="image_url"
                value={formProduct.image_url}
                {...register("image_url")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    image_url: e.target.value,
                  }))
                }
              />

              {errors.image_url && (
                <div className="mt-1">
                  <div className="text-red-500">{errors.image_url.message}</div>
                </div>
              )}
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Stock
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="number"
                min="0"
                placeholder="Stock"
                name="stock"
                value={formProduct.stock}
                {...register("stock")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    stock: e.target.value,
                  }))
                }
                onFocus={(event) => event.target.select()}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
              />

              {errors.stock && (
                <div className="mt-1">
                  <div className="text-red-500">{errors.stock.message}</div>
                </div>
              )}
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Rating
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="number"
                min="0"
                placeholder="Rating"
                name="rating"
                value={formProduct.rating}
                {...register("rating")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    rating: e.target.value,
                  }))
                }
                onFocus={(event) => event.target.select()}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
              />

              {errors.rating && (
                <div className="mt-1">
                  <div className="text-red-500">{errors.rating.message}</div>
                </div>
              )}
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Price
              </label>
              <input
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                type="number"
                min="0"
                placeholder="In IDR"
                name="price"
                value={formProduct.price}
                {...register("price")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    price: e.target.value,
                  }))
                }
                onFocus={(event) => event.target.select()}
                onKeyPress={(event) => {
                  if (!/[0-9]/.test(event.key)) {
                    event.preventDefault();
                  }
                }}
              />

              {errors.price && (
                <div className="mt-1">
                  <div className="text-red-500">{errors.price.message}</div>
                </div>
              )}
            </div>

            <div className="mb-6">
              <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300 capitalize ">
                Status
              </label>
              <select
                className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                name="available"
                value={formProduct.available}
                {...register("available")}
                onChange={(e) =>
                  setFormProduct((prevState) => ({
                    ...prevState,
                    available: e.target.value,
                  }))
                }
              >
                <option defaultValue={1} value={1}>
                  Available
                </option>
                <option value={0}>Not Available</option>
              </select>

              {errors.available && (
                <div className="mt-1">
                  <div className="text-red-500">{errors.available.message}</div>
                </div>
              )}
            </div>

            <button
              type="submit"
              className="text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-auto px-5 py-2.5 text-center bg-purple-700 hover:bg-purple-800"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </AdminLayout>
  );
}
