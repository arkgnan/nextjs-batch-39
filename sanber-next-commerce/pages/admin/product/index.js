import React from "react";
import axios from "axios";
import Cookies from "js-cookie";
import AdminLayout from "../../../layouts/AdminLayout";
import Image from "next/image";
import { useRouter } from "next/router";
export async function getServerSideProps(context) {
  const adminToken = context.req.cookies["token_admin"];

  try {
    const getProduct = await axios(
      `https://service-example.sanbercloud.com/api/product`,
      {
        headers: {
          Authorization: "Bearer " + adminToken,
        },
      }
    );

    const product = await getProduct.data;
    return {
      props: {
        product,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}

export default function Index({ product }) {
  const router = useRouter();
  const handleDelete = async (e) => {
    e.preventDefault();
    const { data } = await axios
      .delete(
        `https://service-example.sanbercloud.com/api/product/${e.target.value}`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      )
      .catch(function (error) {
        if (error.response) {
          throw new Error(error.response.data);
        }
      });
    window?.location.reload("/admin/product");
  };

  return (
    <AdminLayout title="Product">
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-slate-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-slate-700 text-xl font-bold">Product List</h6>
            <button
              className="bg-slate-700 active:bg-slate-600 text-white font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="button"
              onClick={() => router.push("/admin/product/add")}
            >
              Add Product
            </button>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  Name
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  Category
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  Stock
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  Price
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  Status
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"></th>
              </tr>
            </thead>
            <tbody>
              {product?.map((data) => {
                return (
                  <tr key={data.id}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                      <Image
                        src={`/api/imageproxy?url=${encodeURIComponent(
                          data.image_url
                        )}`}
                        alt={data.product_name}
                        width={100}
                        height={100}
                        className="h-12 w-12 bg-white rounded-full border"
                      />{" "}
                      <span className="ml-3 font-bold text-slate-600">
                        {data.product_name}
                      </span>
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.category.category_name}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.stock}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {new Intl.NumberFormat().format(data.price)}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.available == 1 && (
                        <div className="text-green-600">Ready</div>
                      )}
                      {data.available != 1 && (
                        <div className="text-red-600">Out</div>
                      )}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-right">
                      <div className="flex justify-center items-center gap-x-2">
                        <button
                          onClick={(e) =>
                            router.push(`/admin/product/edit/${e.target.value}`)
                          }
                          value={data.id}
                          className="py-1 px-2 rounded-lg bg-emerald-700 text-white"
                        >
                          Edit
                        </button>
                        <button
                          onClick={handleDelete}
                          value={data.id}
                          className="py-1 px-2 rounded-lg bg-rose-700 text-white"
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </AdminLayout>
  );
}
