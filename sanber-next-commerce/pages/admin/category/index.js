import React from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import Cookies from "js-cookie";
import axios from "axios";
import AdminLayout from "../../../layouts/AdminLayout";
import { useRouter } from "next/router";
export async function getServerSideProps(context) {
  const adminToken = context.req.cookies["token_admin"];

  try {
    const { data } = await axios(
      `https://service-example.sanbercloud.com/api/category`,
      {
        headers: {
          Authorization: "Bearer " + adminToken,
        },
      }
    );

    const category = await data;
    return {
      props: {
        category,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}
export default function Index({ category }) {
  const [labelBtn, setLableBtn] = useState("Add");
  const [categoryId, setCategoryId] = useState(null);
  const [categoryName, setCategoryName] = useState("");
  const router = useRouter();
  const {
    handleSubmit,
    formState: { errors },
    register,
    reset,
    clearErrors,
  } = useForm();
  const onSubmit = async (form) => {
    if (labelBtn == "Add") {
      console.log("form", form);
      const { data } = await axios
        .post(`https://service-example.sanbercloud.com/api/category`, form, {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        })
        .catch(function (error) {
          if (error.response) {
            throw new Error(error.response.data);
          }
        });
    } else {
      const { data } = await axios
        .put(
          `https://service-example.sanbercloud.com/api/category/${categoryId}`,
          form,
          {
            headers: {
              Authorization: "Bearer " + Cookies.get("token_admin"),
            },
          }
        )
        .catch(function (error) {
          if (error.response) {
            throw new Error(error.response.data);
          }
        });
    }
    window?.location.reload("/admin/category");
  };
  const handleDelete = async (e) => {
    e.preventDefault();
    setCategoryId(e.target.value);
    const { data } = await axios
      .delete(
        `https://service-example.sanbercloud.com/api/category/${e.target.value}`,
        {
          headers: {
            Authorization: "Bearer " + Cookies.get("token_admin"),
          },
        }
      )
      .catch(function (error) {
        if (error.response) {
          throw new Error(error.response.data);
        }
      });
    window?.location.reload("/admin/category");
  };
  const handleEdit = async (e) => {
    e.preventDefault();
    setCategoryId(e.target.value);
    setLableBtn("Update");
    const { data } = await axios(
      `https://service-example.sanbercloud.com/api/category/${e.target.value}`
    ).catch(function (error) {
      if (error.response) {
        throw new Error(error.response.data);
      }
    });
    setCategoryName(data.category_name);
  };
  console.log(category);
  return (
    <AdminLayout title="Category List">
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-slate-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6">
          <div className="text-center flex justify-between items-center">
            <h6 className="text-slate-700 text-xl font-bold">Category List</h6>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex items-center w-full p-2 gap-x-3">
                <div className="grow">
                  <input
                    className="bg-gray-50 border border-gray-300 text-gray-900
                        text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                        dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    type="text"
                    placeholder="Category name"
                    name="category_name"
                    value={categoryName}
                    {...register("category_name")}
                    onChange={(e) => setCategoryName(e.target.value)}
                  />

                  {errors.category_name && (
                    <div className="mt-1">
                      <div className="text-red-500">
                        {errors.category_name.message}
                      </div>
                    </div>
                  )}
                </div>

                <button
                  type="submit"
                  className="text-white focus:ring-4 focus:outline-none font-medium rounded-lg text-sm w-auto px-5 py-2.5 text-center bg-purple-700 hover:bg-purple-800"
                >
                  {labelBtn}
                </button>
              </div>
            </form>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  ID
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  CODE
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  NAME
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100"></th>
              </tr>
            </thead>
            <tbody>
              {category?.map((data) => {
                return (
                  <tr key={data.id}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                      {data.id}
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.category_code}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.category_name}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-right">
                      <div className="flex justify-center items-center gap-x-2">
                        <button
                          onClick={handleEdit}
                          value={data.id}
                          className="py-1 px-2 rounded-lg bg-emerald-700 text-white"
                        >
                          Edit
                        </button>
                        <button
                          onClick={handleDelete}
                          value={data.id}
                          className="py-1 px-2 rounded-lg bg-rose-700 text-white"
                        >
                          Delete
                        </button>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </AdminLayout>
  );
}
