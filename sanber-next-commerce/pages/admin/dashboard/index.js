import React from "react";
import AdminLayout from "../../../layouts/AdminLayout";

function index(props) {
  return <AdminLayout title="Dashboard" dashboard></AdminLayout>;
}

export default index;
