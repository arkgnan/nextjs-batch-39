import React from "react";
import axios from "axios";
import AdminLayout from "../../layouts/AdminLayout";
import Image from "next/image";
export async function getServerSideProps(context) {
  const adminToken = context.req.cookies["token_admin"];

  try {
    const { data } = await axios(
      `https://service-example.sanbercloud.com/api/checkout`,
      {
        headers: {
          Authorization: "Bearer " + adminToken,
        },
      }
    );

    const checkout = await data;
    return {
      props: {
        checkout,
      },
    };
  } catch (error) {
    return {
      props: {},
    };
  }
}

function checkout({ checkout }) {
  console.log(checkout);
  return (
    <AdminLayout title="Checkout">
      <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-slate-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-slate-700 text-xl font-bold">Checkout</h6>
          </div>
        </div>
        <div className="block w-full overflow-x-auto">
          <table className="items-center w-full bg-transparent border-collapse">
            <thead>
              <tr>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  ID
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  PRODUCT
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  USER
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  NOTE
                </th>
                <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-slate-50 text-slate-500 border-slate-100">
                  DATE
                </th>
              </tr>
            </thead>
            <tbody>
              {checkout?.map((data) => {
                return (
                  <tr key={data.id}>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      {data.id}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                      <Image
                        src={`/api/imageproxy?url=${encodeURIComponent(
                          data.product.image_url
                        )}`}
                        alt={data.product.product_name}
                        width={100}
                        height={100}
                        className="h-12 w-12 bg-white rounded-full border"
                      />{" "}
                      <span className="ml-3 font-bold text-slate-600">
                        {data.product.product_name}
                      </span>
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      <Image
                        src={`/api/imageproxy?url=${encodeURIComponent(
                          data.user.image_url
                        )}`}
                        alt={data.user.name}
                        width={100}
                        height={100}
                        className="h-12 w-12 bg-white rounded-full border"
                      />{" "}
                      <span className="ml-3 font-bold text-slate-600">
                        {data.user.name}
                      </span>
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      Quantity : {data.quantity}
                      <br />
                      Price : {new Intl.NumberFormat().format(data.unit_price)}
                    </td>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                      <div className="flex">
                        {new Date(data.updated_at).toLocaleString("id-ID")}
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </AdminLayout>
  );
}

export default checkout;
